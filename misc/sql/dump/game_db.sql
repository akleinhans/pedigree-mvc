-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 20. Aug 2019 um 16:12
-- Server-Version: 5.7.27-0ubuntu0.18.04.1
-- PHP-Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `game_db`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `developer`
--

CREATE TABLE `developer` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `developer`
--

INSERT INTO `developer` (`id`, `name`, `image_id`) VALUES
(23, 'DICE', 25),
(24, 'Level 5', 26);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `game`
--

CREATE TABLE `game` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `text` text COLLATE utf8_bin NOT NULL,
  `image_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
  `developer_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `game`
--

INSERT INTO `game` (`id`, `name`, `text`, `image_id`, `publisher_id`, `developer_id`, `created`, `updated`) VALUES
(21, 'NEUNEU', '<p>Tets <strong>stesd</strong> f <span style=\"color: #e03e2d;\">dsf dsfds</span>ü</p>', 33, 22, 23, '2019-08-13 11:56:10', '2019-08-20 13:13:38'),
(22, 'Nier Automata', '<p>Super <span style=\"color: #3598db;\"><strong>Game</strong></span> usw.</p>', 34, 23, 23, '2019-08-13 12:01:41', '2019-08-13 12:01:41'),
(27, 'dsfgdfgfdsg', '<p>&lt;bla&gt;üöä&lt;/bla&gt;</p>', 0, 23, 24, '2019-08-20 13:23:14', '2019-08-20 13:23:14');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `game_genre`
--

CREATE TABLE `game_genre` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `game_genre`
--

INSERT INTO `game_genre` (`id`, `game_id`, `genre_id`) VALUES
(1, 21, 19),
(2, 21, 20),
(3, 21, 21),
(4, 22, 20),
(5, 22, 21),
(6, 22, 26),
(7, 22, 27),
(22, 27, 21),
(23, 27, 26),
(24, 27, 27),
(25, 27, 28);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `game_platform`
--

CREATE TABLE `game_platform` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `game_platform`
--

INSERT INTO `game_platform` (`id`, `game_id`, `platform_id`) VALUES
(1, 21, 1),
(2, 21, 4),
(3, 22, 1),
(4, 22, 2),
(9, 27, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `genre`
--

CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `genre`
--

INSERT INTO `genre` (`id`, `name`, `image_id`) VALUES
(19, 'Action', 0),
(20, 'Adventure', 0),
(21, 'Fighting', 0),
(23, 'Platform', 0),
(24, 'Puzzle', 0),
(25, 'Racing', 0),
(26, 'Role-Playing', 0),
(27, 'Shooter', 0),
(28, 'Simulation', 0),
(29, 'Sports', 0),
(30, 'Strategy', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `images`
--

INSERT INTO `images` (`id`, `name`, `image`, `thumb`, `size`) VALUES
(1, 'nier-automata-cover.jpg', 'uploads/images/2019/08/05/1565011338-8899.jpg', 'uploads/images/2019/08/05/1565011338-8899-thumb.jpg', 111909),
(2, 'nier-automata-cover.jpg', 'uploads/images/2019/08/05/1565011522-9906.jpg', 'uploads/images/2019/08/05/1565011522-9906-thumb.jpg', 111909),
(4, 'nier-automata-cover.jpg', 'uploads/images/2019/08/05/1565011892-9928.jpg', 'uploads/images/2019/08/05/1565011892-9928-thumb.jpg', 111909),
(5, 'nier-automata-cover.jpg', 'uploads/images/2019/08/05/1565011911-4929.jpg', 'uploads/images/2019/08/05/1565011911-4929-thumb.jpg', 111909),
(11, '1565000606-8443.png', 'uploads/images/2019/08/06/1565095038-1412.png', 'uploads/images/2019/08/06/1565095038-1412-thumb.png', 689464),
(12, 'flame.png', 'uploads/images/2019/08/06/1565095062-1735.png', 'uploads/images/2019/08/06/1565095062-1735-thumb.png', 47227),
(13, 'flash.png', 'uploads/images/2019/08/06/1565095291-1958.png', 'uploads/images/2019/08/06/1565095291-1958-thumb.png', 64630),
(19, 'flash.png', 'uploads/images/2019/08/06/1565097647-7617.png', 'uploads/images/2019/08/06/1565097647-7617-thumb.png', 64630),
(22, 'flash.png', 'uploads/images/2019/08/06/1565097871-1672.png', 'uploads/images/2019/08/06/1565097871-1672-thumb.png', 64630),
(23, 'EA.png', 'uploads/images/2019/08/06/1565098829-3328.png', 'uploads/images/2019/08/06/1565098829-3328-thumb.png', 3514),
(24, 'Ubisoft.png', 'uploads/images/2019/08/06/1565098878-6339.png', 'uploads/images/2019/08/06/1565098878-6339-thumb.png', 52348),
(25, 'dice.png', 'uploads/images/2019/08/06/1565098935-3776.png', 'uploads/images/2019/08/06/1565098935-3776-thumb.png', 1054),
(26, 'level5.png', 'uploads/images/2019/08/06/1565098980-3916.png', 'uploads/images/2019/08/06/1565098980-3916-thumb.png', 13925),
(28, 'xbox.jpg', 'uploads/images/2019/08/08/1565265171-5447.jpg', 'uploads/images/2019/08/08/1565265171-5447-thumb.jpg', 19002),
(29, 'playstaion.jpeg', 'uploads/images/2019/08/08/1565265190-4846.jpg', 'uploads/images/2019/08/08/1565265190-4846-thumb.jpg', 121132),
(31, 'nintendo.jpg', 'uploads/images/2019/08/08/1565265327-6515.jpg', 'uploads/images/2019/08/08/1565265327-6515-thumb.jpg', 30049),
(32, 'EA.png', 'uploads/images/2019/08/12/1565612892-8578.png', 'uploads/images/2019/08/12/1565612892-8578-thumb.png', 3514),
(33, 'YoRHa_No.2_Type_B.png', 'uploads/images/2019/08/13/1565697369-8524.png', 'uploads/images/2019/08/13/1565697369-8524-thumb.png', 5062730),
(34, 'YoRHa_No.2_Type_B_SMALL.png', 'uploads/images/2019/08/13/1565697701-5598.png', 'uploads/images/2019/08/13/1565697701-5598-thumb.png', 689464);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `platform`
--

CREATE TABLE `platform` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `image_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `platform`
--

INSERT INTO `platform` (`id`, `name`, `image_id`) VALUES
(1, 'XBOX', '28'),
(2, 'PlayStation', '29'),
(4, 'Nintendo', '31');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `publisher`
--

CREATE TABLE `publisher` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `publisher`
--

INSERT INTO `publisher` (`id`, `name`, `image_id`) VALUES
(22, 'EA', 23),
(23, 'Ubisoft', 24);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `nickname` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `nickname`, `email`, `password`, `role`, `created`, `updated`) VALUES
(1, 'Niklas', 'Berend', 'niki_danger', 'nberend@sae.edu', '$2y$10$Jzsc8qkKxJ8xw5ui6zcd3uqfFKe3259TBdByk9vhfHtPZXUPgr2H6', NULL, '2019-07-22 12:09:41', '2019-07-22 12:09:41');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `developer`
--
ALTER TABLE `developer`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `game_genre`
--
ALTER TABLE `game_genre`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `game_platform`
--
ALTER TABLE `game_platform`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indizes für die Tabelle `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `platform`
--
ALTER TABLE `platform`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `developer`
--
ALTER TABLE `developer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT für Tabelle `game`
--
ALTER TABLE `game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT für Tabelle `game_genre`
--
ALTER TABLE `game_genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT für Tabelle `game_platform`
--
ALTER TABLE `game_platform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT für Tabelle `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT für Tabelle `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT für Tabelle `platform`
--
ALTER TABLE `platform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `publisher`
--
ALTER TABLE `publisher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
