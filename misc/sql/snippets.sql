

#########################
SELECT p.*, i.name AS image_name, i.image, i.thumb, i.size
FROM publisher AS p
         LEFT JOIN images AS i ON i.id = p.image_id
WHERE p.id = :id;

###################################
SELECT p.*, i.name AS image_name, i.image, i.thumb, i.size
FROM publisher AS p
         LEFT JOIN images AS i ON i.id = p.image_id
WHERE p.id = :id;