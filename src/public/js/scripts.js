// scripts.js


// SEMANTIC_UI CONFIG --------------------------------------------------------------------------------------------------
$('.tabular.menu .item').tab();

$('select.dropdown').dropdown();



// TINY-MCE CONFIG -----------------------------------------------------------------------------------------------------
tinymce.init({
    selector: '.tinymce-editor',
    entity_encoding: "raw",
});


