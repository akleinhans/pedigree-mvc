<?php


error_reporting(E_ALL);
ini_set('display_errors', 1);

//----------------------------------------------------------------------------------------------------------------------
// MySQL Settings

define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_PORT', '8888');


define('DB_NAME', 'pedigree_db');
define('DB_USER', 'pedigree_db');
define('DB_PASS', 'pedigree_pw');

define('DB_CHARSET', 'utf8');

//----------------------------------------------------------------------------------------------------------------------

define('URL', '/pedigree-mvc/');

//----------------------------------------------------------------------------------------------------------------------

define('IMAGE_UPLOADS_PATH', 'uploads/images');
define('IMAGE_THUMB_NAME', '-thumb');
define('IMAGE_THUMB_EXT', '.jpg');

define('IMAGE_THUMB_WIDTH', 200);
define('IMAGE_THUMB_HEIGHT', 200);

define('IMAGE_PLACEHOLDER', 'public/assets/image/placeholder.png');
define('IMAGE_PLACEHOLDER_THUMB', 'public/assets/image/placeholder-thumb.png');





