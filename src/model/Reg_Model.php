<?php


class Reg_Model extends Model {

    function loginUser($formData) {

        $user = $this->getUserFromEmail($formData['email']);

        if ($user) {

            $result = password_verify($formData['password'],  $user['password']);

            if (!$result) {
                return false;
            }

            unset($user['password']);

            return $user;
        } else {

            return false;
        }
    }

    function getUserFromEmail($email) {
        $sql = "SELECT * FROM user WHERE  email = :email LIMIT 1;";

        $obj = $this->db->prepare($sql);
        $obj->execute(array(':email' => $email));

        $result = $obj->fetch(PDO::FETCH_ASSOC);

        return $result;

    }

    function userExists($email) {
        return (!!$this->getUserFromEmail($email));
    }

    function validateForm($formData) {

            $error = array();

            foreach ($formData as $key => $value) {
                if (empty($value)) $error[$key] = 'This field is empty';

                switch ($key) {
                    case 'email':
                        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                            $error[$key] = "This is not an valid Email";
                        } else {
                            if($this->userExists($value)) $error[$key] = 'Email already taken';
                        }
                        break;
                    case 'password':
                        if (strlen($formData[$key]) < 6)
                            $error[$key] = "password is too short";
                        break;
                    case 'password2':
                        if ($formData['password'] != $formData['password2'])
                            $error[$key] = "password is not the same";
                        break;
                }
            }
            return !empty($error) ? $error : false;
    }

    function registerUser($user) {

            $sql = 'INSERT INTO user (firstname, lastname, street, postalcode, residence, phone, email, password) VALUES (:firstname, :lastname, :street, :postalcode, :residence, :phone, :email, :password)';

            $stmt = $this->db->prepare($sql);

            $hashedPassword = password_hash($user['password'], PASSWORD_DEFAULT);

            $result = $stmt->execute(array(
            ':firstname' => $user['firstname'],
            ':lastname' => $user['lastname'],
            ':street' => $user['street'],
            ':postalcode' => $user['postalcode'],
            ':residence' => $user['residence'],
            ':phone' => $user['phone'],
            ':email' => $user['email'],
            ':password' => $hashedPassword
            ));

            return $result;
        }

}