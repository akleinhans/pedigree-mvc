<?php


class Breeder_Model extends Model {

    public function getAllBreeder($search = '')
    {
        $sql = "
                SELECT breeder.*, image.thumb
                FROM breeder
                LEFT JOIN image ON breeder.image_id = image.id
                WHERE breeder.name LIKE :search;
                ";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(':search' => "%$search%"));

        $breeder = $obj->fetchAll(PDO::FETCH_ASSOC);

        return $breeder;
    }

    /*###################################################*/

    public function getAllFromTable($table_name) {

        switch($table_name) {
            case 'name':
                $sql = "SELECT * FROM name;";
                break;
            case 'lastname':
                $sql = "SELECT * FROM lastname;";
                break;
            case 'street':
                $sql = "SELECT * FROM street;";
                break;
            case 'residence':
                $sql = "SELECT * FROM residence;";
                break;
            case 'phone':
                $sql = "SELECT * FROM phone;";
                break;
            case 'email':
                $sql = "SELECT * FROM email;";
                break;

            default:
                return false;
        }

        $obj = $this->db->prepare($sql);

        $obj->execute();

        $result = $obj->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function insertImage($uploaded_file) {

        $sql = 'INSERT INTO image (name, image, thumb, size) VALUES (:name, :image, :thumb, :size);';

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $uploaded_file['name'],
            ':image' => $uploaded_file['image'],
            ':thumb' => $uploaded_file['thumb'],
            ':size' => $uploaded_file['size']
        ));

        return ($result) ? $this->db->lastInsertId() : false;

    }

    public function addBreeder($formData, $uploaded_file) {

        $image_id = ($uploaded_file) ? $this->insertImage($uploaded_file) : 0;

        $sql = "INSERT INTO breeder (name, lastname, street, residence, phone, email, image_id) 
                VALUES (:name, :lastname, :street, :residence, :phone, :email, :image_id)";

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $formData['name'],
            ':lastname' => $formData['lastname'],
            ':street' => $formData['street'],
            ':residence' => $formData['residence'],
            ':image_id' => $image_id,
            ':phone' => $formData['phone'],
            ':email' => $formData['email']
        ));

        return $result;
    }
    public function getDetailForBreeder($breeder_id) {

        $sql = "
                SELECT breeder.*, image.name AS image_name, image.image, image.thumb, image.size
                FROM breeder 
                LEFT JOIN image ON image.id = breeder.image_id
                WHERE breeder.id = :breeder_id
                ";

        $obj = $this->db->prepare($sql);
        $obj->execute(array(':breeder_id' => $breeder_id));
        $result = $obj->fetchAll(PDO::FETCH_ASSOC)[0];


        return $result;
    }


    public function deleteImagesForBreeder($breeder_id) {

        $sql = "
                SELECT breeder.image_id, image.image, image.thumb
                FROM breeder
                LEFT JOIN image ON image.id = breeder.image_id
                WHERE breeder.id = :breeder_id;
                ";

        $obj = $this->db->prepare($sql);
        $obj->execute(array(':breeder_id' => $breeder_id));
        $data = $obj->fetch(PDO::FETCH_ASSOC);
        // delete files
        File::delete($data['image']);
        File::delete($data['thumb']);
        // delete from image table
        $obj2 = $this->db->prepare('DELETE FROM image WHERE id = :image_id');
        $result = $obj2->execute(array(':image_id' => $data['image_id']));

        return $result;
    }

    public function deleteBreeder($breeder_id) {

        // 2. images
        $this->deleteImagesForBreeder($breeder_id);
        // 3. dog
        $sql = "
                DELETE FROM breeder
                WHERE id = :breeder_id
                ";

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(':breeder_id' => $breeder_id));

        return $result;
    }
}