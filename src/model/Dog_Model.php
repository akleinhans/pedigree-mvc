<?php


class Dog_Model extends Model {

    public function getAllDogs($search = '') {

        $sql = "
                SELECT dog.*, image.name AS image_name, image.thumb, image.size, 
                            gender.name AS gender_name, 
                            colour.name AS colour_name
                FROM dog 
                LEFT JOIN image ON image.id = dog.image_id
                LEFT JOIN gender ON gender.id = dog.gender_id
                LEFT JOIN colour ON colour.id = dog.colour_id
                WHERE dog.name LIKE :search OR dog.text LIKE :search;
                ";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(':search' => "%$search%"));

        $dogs = $obj->fetchAll(PDO::FETCH_ASSOC);

        // add disease and titles for all dogs
        foreach ($dogs as $index => $dog) {
            $dogs[$index]['disease'] = $this->getPropsForDog('disease', $dog['id']);
            $dogs[$index]['titles'] = $this->getPropsForDog('titles', $dog['id']);
        }

        return $dogs;
    }

    public function getAllFromTable($table_name) {

        switch($table_name) {
            case 'colour':
                $sql = "SELECT * FROM colour;";
                break;
            case 'gender':
                $sql = "SELECT * FROM gender;";
                break;
            case 'disease':
                $sql = "SELECT * FROM disease;";
                break;
            case 'titles':
                $sql = "SELECT * FROM titles;";
                break;
            case 'breed':
                $sql = "SELECT * FROM breed;";
                break;
            case 'breeder':
                $sql = "SELECT * FROM breeder;";
                break;
            case 'owner':
                $sql = "SELECT * FROM owner;";
                break;
            case 'association':
                $sql = "SELECT * FROM association;";
                break;
            case 'birthdate':
                $sql = "SELECT * FROM birthdate;";
                break;
            case 'registration':
                $sql = "SELECT * FROM registration;";
                break;
            default:
                return false;
        }

        $obj = $this->db->prepare($sql);

        $obj->execute();

        $result = $obj->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getPropsForDog($type, $dog_id) {

        switch ($type) {
            case 'disease':
                $sql = "
                        SELECT gg.disease_id AS id, g.name
                        FROM dog_disease AS gg
                        LEFT JOIN disease AS g ON g.id = gg.disease_id
                        WHERE dog_id = :dog_id;
                        ";
                break;
            case 'titles':
                $sql = "
                        SELECT gp.titles_id AS id, p.name
                        FROM dog_titles AS gp
                        LEFT JOIN titles AS p ON p.id = gp.titles_id
                        WHERE dog_id = :dog_id;
                        ";
                break;
        }

        $obj = $this->db->prepare($sql);
        $obj->execute(array(':dog_id' => $dog_id));
        $result = $obj->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function insertImage($uploaded_file) {

        $sql = 'INSERT INTO image (name, image, thumb, size) VALUES (:name, :image, :thumb, :size);';

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $uploaded_file['name'],
            ':image' => $uploaded_file['image'],
            ':thumb' => $uploaded_file['thumb'],
            ':size' => $uploaded_file['size']
        ));

        return ($result) ? $this->db->lastInsertId() : false;

    }

    public function addProps($type, $dog_id, $array_data) {

        switch($type) {
            case 'disease':
                $sql = "INSERT INTO dog_disease (dog_id, disease_id) VALUE (:dog_id, :value_id);";
                break;

            case 'titles':
                $sql = "INSERT INTO dog_titles (dog_id, titles_id) VALUE (:dog_id, :value_id);";
                break;
        }

        foreach ($array_data as $value_id) {
            $obj = $this->db->prepare($sql);

            $obj->execute(array(
                ':dog_id' => $dog_id,
                ':value_id' => $value_id
            ));
        }

    }

    public function addDog($formData, $uploaded_file) {

        $image_id = ($uploaded_file) ? $this->insertImage($uploaded_file) : 0;

        $sql = "INSERT INTO dog(gender_id, name, Nickname, birth_date, colour_id, association_id, reg_id, father_id, mother_id, image_id, breed_id, owner_id, breeder_id, text) 
                VALUES (:gender_id, :name, :Nickname, :birth_date, :colour_id, :association_id, :reg_id, :father_id, :mother_id, :image_id, :breed_id, :owner_id, :breeder_id, :text)";

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(

            ':gender_id' => $formData['gender'],
            ':name' => $formData['name'],
            ':Nickname' => $formData['Nickname'],
            ':birth_date' => $formData['birth_date'],
            ':colour_id' => $formData['colour'],
            ':association_id' => $formData['association'],
            ':reg_id' => $formData['reg_id'],
            ':father_id' => $formData['father_id'],
            ':mother_id' => $formData['mother_id'],
            ':image_id' => $image_id,
            ':breed_id' => $formData['breed'],
            ':owner_id' => $formData['owner'],
            ':breeder_id' => $formData['breeder'],
            ':text' => $formData['text'],
        ));

        $dog_id = $this->db->lastInsertId();

        $this->addProps('titles', $dog_id, $formData['titles']);
        $this->addProps('disease', $dog_id, $formData['disease']);
        /*$this->addProps('sibling', $dog_id, $formData['sibling']);*/

        return $result;

    }
    public function getDetailForDog($dog_id) {

        $sql = "
                SELECT dog.*, image.name AS image_name, image.image, image.thumb, image.size, 
                            gender.name AS gender_name, 
                            colour.name AS colour_name,
                            breed.name AS breed_name,
                            association.name AS association_name,
                            breeder.name AS breeder_name,
                            owner.name AS owner_name
                FROM dog 
                
                LEFT JOIN image ON image.id = dog.image_id
                LEFT JOIN gender ON gender.id = dog.gender_id
                LEFT JOIN colour ON colour.id = dog.colour_id
                LEFT JOIN breed ON breed.id = dog.breed_id
                LEFT JOIN association ON association.id = dog.association_id
                LEFT JOIN breeder ON breeder.id = dog.breeder_id
                LEFT JOIN owner ON owner.id = dog.owner_id
                WHERE dog.id = :dog_id
                ";

        $obj = $this->db->prepare($sql);
        $obj->execute(array(':dog_id' => $dog_id));

        $dog = $obj->fetchAll(PDO::FETCH_ASSOC)[0];

        $dog['disease'] = $this->getPropsForDog('disease', $dog_id);
        $dog['titles'] = $this->getPropsForDog('titles', $dog_id);

        return $dog;
    }

    public function deleteImagesForDog($dog_id) {

        $sql = "
                SELECT dog.image_id, image.image, image.thumb
                FROM dog
                LEFT JOIN image ON image.id = dog.image_id
                WHERE dog.id = :dog_id;
                ";

        $obj = $this->db->prepare($sql);
        $obj->execute(array(':dog_id' => $dog_id));
        $data = $obj->fetch(PDO::FETCH_ASSOC);
        // delete files
        File::delete($data['image']);
        File::delete($data['thumb']);
        // delete from image table
        $obj2 = $this->db->prepare('DELETE FROM image WHERE id = :image_id');
        $result = $obj2->execute(array(':image_id' => $data['image_id']));

        return $result;

    }
    //eltern bzw geschwister
    public function deletePropsForDog($type, $dog_id) {
        switch ($type) {
            case 'disease':
                $sql = "
                        DELETE FROM dog_disease
                        WHERE dog_id = :dog_id;
                        ";
                break;
            case 'titles':
                $sql = "
                        DELETE FROM dog_titles
                        WHERE dog_id = :dog_id;
                        ";
                break;
        }

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':dog_id' => $dog_id
        ));

        return $result;
    }

    public function deleteDog($dog_id) {
        // 1. props
        $this->deletePropsForDog('disease', $dog_id );
        $this->deletePropsForDog('titles', $dog_id );
        // 2. images
        $this->deleteImagesForDog($dog_id);
        // 3. dog
        $sql = "
                DELETE FROM dog
                WHERE id = :dog_id
                ";

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(':dog_id' => $dog_id));

        return $result;
    }


}

