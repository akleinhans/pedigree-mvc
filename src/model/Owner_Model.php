<?php


class Owner_Model extends Model {

    public function getAllOwner($search = '') {

        $sql = "
                SELECT owner.*, image.thumb
                FROM owner
                LEFT JOIN image ON owner.image_id = image.id
                WHERE owner.name LIKE :search;
                ";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(':search' => "%$search%"));

        $owner = $obj->fetchAll(PDO::FETCH_ASSOC);

        return $owner;
    }
    //------------INDEX-------------------------------------------------------------------------------------------------

    public function getAllFromTable($table_name) {

        switch($table_name) {
            case 'name':
                $sql = "SELECT * FROM name;";
                break;
            case 'street':
                $sql = "SELECT * FROM street;";
                break;
            case 'residence':
                $sql = "SELECT * FROM residence;";
                break;
            case 'phone':
                $sql = "SELECT * FROM phone;";
                break;
            case 'email':
                $sql = "SELECT * FROM email;";
                break;

            default:
                return false;
        }

        $obj = $this->db->prepare($sql);

        $obj->execute();

        $result = $obj->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }


    public function insertImage($uploaded_file) {

        $sql = 'INSERT INTO image (name, image, thumb, size) VALUES (:name, :image, :thumb, :size);';

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $uploaded_file['name'],
            ':image' => $uploaded_file['image'],
            ':thumb' => $uploaded_file['thumb'],
            ':size' => $uploaded_file['size']
        ));

        return ($result) ? $this->db->lastInsertId() : false;
    }

    public function addOwner($formData, $uploaded_file) {

        $image_id = ($uploaded_file) ? $this->insertImage($uploaded_file) : 0;

        $sql = "INSERT INTO owner (name, street, residence, phone, email, image_id) 
                VALUES (:name, :street, :residence, :phone, :email, :image_id);";

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $formData['name'],
            ':street' => $formData['street'],
            ':residence' => $formData['residence'],
            ':phone' =>  $formData['phone'],
            ':email' =>  $formData['email'],
            ':image_id' => $image_id
        ));

        return $result;
    }

    public function getDetailForOwner($owner_id) {

        $sql = "
               SELECT owner.*, image.name AS image_name, image.image, image.thumb, image.size
                FROM owner
                LEFT JOIN image ON image.id = owner.image_id
                WHERE owner.id = :owner_id
                ";

        $obj = $this->db->prepare($sql);
        $obj->execute(array(':owner_id' => $owner_id));

        $result = $obj->fetchAll(PDO::FETCH_ASSOC)[0];


        return $result;
    }

    public function deleteImagesForOwner($owner_id) {

        $sql = "
                SELECT owner.image_id, image.image, image.thumb
                FROM owner
                LEFT JOIN image ON image.id = owner.image_id
                WHERE owner.id = :owner_id;
                ";

        $obj = $this->db->prepare($sql);
        $obj->execute(array(':owner_id' => $owner_id));
        $data = $obj->fetch(PDO::FETCH_ASSOC);
        // delete files
        File::delete($data['image']);
        File::delete($data['thumb']);
        // delete from image table
        $obj2 = $this->db->prepare('DELETE FROM image WHERE id = :image_id');
        $result = $obj2->execute(array(':image_id' => $data['image_id']));

        return $result;

    }

    public function deleteOwner($owner_id) {

        // 2. images
        $this->deleteImagesForOwner($owner_id);
        // 3. association
        $sql = "
                DELETE FROM owner
                WHERE id = :owner_id
                ";

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(':owner_id' => $owner_id));

        return $result;
    }

}