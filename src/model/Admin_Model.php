<?php


class Admin_Model extends Model {
    //funktioniert
    public function insertImage($uploaded_file) {
        $sql = 'INSERT INTO image (name, image, thumb, size) VALUES (:name, :image, :thumb, :size)';

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $uploaded_file['name'],
            ':image' => $uploaded_file['image'],
            ':thumb' => $uploaded_file['thumb'],
            ':size' => $uploaded_file['size'],
        ));

        return ($result) ? $this->db->lastInsertId() : false;
    }

    public function insertRowIntoTable($tablename, $name, $uploaded_file) {

        $image_id = ($uploaded_file) ? $this->insertImage($uploaded_file) : 0;

        switch ($tablename) {
            case 'breed':
                $sql = 'INSERT INTO breed (name, image_id) VALUES (:name, :image_id);';
                break;
            case 'colour':
                $sql = 'INSERT INTO colour (name, image_id) VALUES (:name, :image_id);';
                break;
            case 'disease':
                $sql = 'INSERT INTO disease (name, image_id) VALUES (:name, :image_id);';
                break;
            case 'gender':
                $sql = 'INSERT INTO gender (name, image_id) VALUES (:name, :image_id);';
                break;
            case 'titles':
                $sql = 'INSERT INTO titles (name, image_id) VALUES (:name, :image_id);';
                break;


            default: return false;
        }

        $obj = $this->db->prepare($sql);
        $result = $obj->execute(array(
            ':name' => $name,
            ':image_id' => $image_id,

        ));

        return $result;
    }

    public function updateRowFromTable($tablename, $id, $name, $uploaded_file) {

        $item = $this->getRowFromTable($tablename, $id);

        if ($uploaded_file) {
            //delete image
            if ($item['image_name'])
                $this->deleteImageEntryAndFiles($item['image_id'], $item['image'], $item['thumb']);
            //new image
                $image_id = $this->insertImage($uploaded_file);
        } else {
            //use old image
            $image_id = $item['image_id'];
        }

        switch ($tablename) {
            case 'colour':
                $sql = 'UPDATE colour SET name = :name, image_id = :image_id WHERE id = :id;';
                break;
            case 'breed':
                $sql = 'UPDATE breed SET name = :name, image_id = :image_id WHERE id = :id;';
                break;
            case 'disease':
                $sql = 'UPDATE disease SET name = :name, image_id = :image_id WHERE id = :id;';
                break;
            case 'gender':
                $sql = 'UPDATE gender SET name = :name, image_id = :image_id WHERE id = :id;';
                break;
            case 'titles':
                $sql = 'UPDATE titles SET name = :name, image_id = :image_id WHERE id = :id;';
                break;

            default: return false;
        }
        $obj = $this->db->prepare($sql);
        $result = $obj->execute(array(
            ':name' => $name,
            ':id' => $id,
            ':image_id' => $image_id
        ));

        return $result;
    }

    public function getRowFromTable($tablename, $id) {

        switch ($tablename) {
            case 'colour':
                $clean_table = 'colour';
                break;
            case 'breed':
                $clean_table = 'breed';
                break;
            case 'disease':
                $clean_table = 'disease';
                break;
            case 'gender':
                $clean_table = 'gender';
                break;
            case 'titles':
                $clean_table = 'titles';
                break;


            default:
                return false;
        }
                // no inspection needed
        $sql = "SELECT p.*, i.name AS Image_name, i.image, i.thumb, i.size 
                FROM $clean_table AS p 
                LEFT JOIN image AS i 
                ON i.id = p.image_id 
                WHERE p.id = :id;";

        $obj = $this->db->prepare($sql);
        $obj->execute(array(':id' => $id));
        $result = $obj->fetch(PDO::FETCH_ASSOC);

        return $result;

    }

    public function deleteImageEntryAndFiles($id, $image, $thumb) {

        //delete image
        File::delete($image);
        File::delete($thumb);

        $obj = $this->db->prepare('DELETE FROM image WHERE id = :id');
        $result = $obj->execute(array(':id' => $id));

        return $result;
    }

    public function deleteFromTable($tablename, $id) {

        $item = $this->getRowFromTable($tablename, $id);

        if ($item['image_id'])
            $this->deleteImageEntryAndFiles($item['image_id'], $item['image'], $item['thumb']);

        switch ($tablename) {
            case 'association':
                $sql = 'DELETE FROM association WHERE id = :id;';
                break;
            case 'breed':
                $sql = 'DELETE FROM breed WHERE id = :id;';
                break;
            case 'colour':
                $sql = 'DELETE FROM colour WHERE id = :id;';
                break;
            case 'disease':
                $sql = 'DELETE FROM disease WHERE id = :id;';
                break;
            case 'gender':
                $sql = 'DELETE FROM gender WHERE id = :id;';
                break;



            default: return false;
        }

        $obj = $this->db->prepare($sql);
        $result = $obj->execute(array(':id' => $id));

        return $result;
    }


    public function getAllFromTable($tablename) {

        switch ($tablename) {
            case 'breed':
                $clean_sql = 'breed';
                break;
            case 'colour':
                $clean_sql = 'colour';
                break;
            case 'disease':
                $clean_sql = 'disease';
                break;
            case 'gender':
                $clean_sql = 'gender';
                break;
            case 'titles':
                $clean_sql = 'titles';
                break;

            default: return false;
        }

        /** @noinspection SqlResolve */
        $sql = "SELECT x.*, i.thumb FROM $clean_sql AS x LEFT JOIN image AS i ON i.id = x.image_id WHERE 1;";

        $result = $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

}