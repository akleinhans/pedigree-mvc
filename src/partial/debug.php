

<div class="ui top attached tabular menu">

    <a class="item active" data-tab="empty">Empty</a>

    <a class="item" data-tab="debug">Debug</a>
    <a class="item" data-tab="this">$this</a>
    <a class="item" data-tab="get">$_GET</a>
    <a class="item" data-tab="post">$_POST</a>
    <a class="item" data-tab="session">$_SESSION</a>
    <a class="item" data-tab="files">$_FILES</a>

    <?php foreach (Debug::$data as $key => $value): ?>
        <a class="item" data-tab="<?= is_numeric($key) ? 'index'.$key : $key  ?>"><?= is_numeric($key) ? 'Index: '.$key : $key  ?></a>
    <?php endforeach; ?>

</div>

<div class="ui bottom attached tab segment active" data-tab="empty">
    <p>Empty</p>
</div>

<div class="ui bottom attached tab segment" data-tab="debug">
    <?php Helper::debug(Debug::$data); ?>
</div>

<div class="ui bottom attached tab segment" data-tab="this">
    <?php Helper::debug($this); ?>
</div>

<div class="ui bottom attached tab segment" data-tab="get">
    <?php Helper::debug($_GET); ?>
</div>

<div class="ui bottom attached tab segment" data-tab="post">
    <?php Helper::debug($_POST); ?>
</div>

<div class="ui bottom attached tab segment" data-tab="session">
    <?php Helper::debug($_SESSION); ?>
</div>

<div class="ui bottom attached tab segment" data-tab="files">
    <?php Helper::debug($_FILES); ?>
</div>


<?php foreach (Debug::$data as $key => $value): ?>
    <div class="ui bottom attached tab segment" data-tab="<?= is_numeric($key) ? 'index'.$key : $key ?>">
        <?php Helper::debug($value); ?>
    </div>
<?php endforeach; ?>