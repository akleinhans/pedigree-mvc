<?php

    $controller = Session::get('controller');

    $user = Session::get('user');

    $search = isset($_GET['search']) ? trim($_GET['search']) : '';

?>

<div class="ui fixed inverted menu">
    <div class="ui container">

            <a class="<?= ($controller == 'Home') ? 'active' : '' ?> item" href="<?=URL?>home">
                <img class="logo" src="<?=URL?>public/pictures/aussiePedigree.png" alt="Pedigree Logo">
            </a>
            <a class="<?= ($controller == 'Dog') ? 'active' : '' ?> item" href="<?=URL?>dog">
                Dogs
            </a>
            <a class="<?= ($controller == 'Breeder') ? 'active' : '' ?> item" href="<?=URL?>breeder">
                Breeder
            </a>
            <a class="<?= ($controller == 'Owner') ? 'active' : '' ?> item" href="<?=URL?>owner">
                Owner
            </a>
            <a class="<?= ($controller == 'Association') ? 'active' : '' ?> item" href="<?=URL?>association">
                Association
            </a>


        <div class="right menu">
            <div class="item white">
                <form action="<?=URL?>dog/search" method="get">
                    <div class="ui icon input">
                        <input type="text" name="search" placeholder=" Search..." value="<?=$search?>">
                        <i class="inverted black search link icon"></i>
                    </div>
                </form>
            </div>

            <?php if ($user): ?>
                <p class="ui item" style="margin-bottom: 0px;"> <?=$user['lastname']?> </p>

                <a class="ui item" href="<?=URL?>reg/doLogout">
                    Logout
                </a>
            <?php else: ?>
                <a class="ui item" href="<?=URL?>reg/logout">
                    Login
                </a>
            <?php endif; ?>
            <a class="<?= ($controller == 'Admin') ? 'active' : '' ?> item" href="<?=URL?>admin">
                Admin
            </a>
        </div>
    </div>
</div>

