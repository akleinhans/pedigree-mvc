<!DOCTYPE html>
<html lang="de" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Pedigree-MVC</title>

    <!-- JS -->

    <script src="<?=URL?>public/lib/jquery/jquery-3.1.1.min.js"></script>
    <script src="<?=URL?>public/lib/semantic-ui/semantic.min.js"></script>

    <script src="<?=URL?>public/lib/tinymce/tinymce.min.js"></script>

    <!-- CSS -->

    <link href="<?=URL?>public/lib/semantic-ui/semantic.min.css"  rel="stylesheet" type="text/css" >

    <link href="<?=URL?>public/css/styles.css" rel="stylesheet" type="text/css" >

</head>
<body>

<div class="ui main container">
