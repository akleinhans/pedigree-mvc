<?php


class Application {

    public function __construct() {

        // URL parsing
        $url = (isset($_GET['url'])) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);

        if (empty($url[0])) $url[0] = 'home';
        $url[0] = ucfirst($url[0]);

        //---------------------------------------------------------------------------

        //TODO: think about 404

        // load controller
        $file = "controller/$url[0].php";
        if (file_exists($file)) {
            require $file;
        } else {
            echo "(404) No \"$url[0]\" controller found <br/>";
            /*
            $view404 = new View();
            $view404->render('404');
            return;
            */
        }

        // create controller
        $controller = new $url[0]();

        //---------------------------------------------------------------------------

        // load model (if existant)
        $controller->loadModel();

        //---------------------------------------------------------------------------
        //TODO: think about 404

        // Method call
        if (isset($url[2])) {
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2]);
            } else {
                echo "Error calling method with param $url[1]($url[2])  <br/>";
                /*
                $view404 = new View();
                $view404->render('404');
                return;
                */
            }
        } else {
            if (isset($url[1])) {
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}();
                } else {
                   echo "Error calling method $url[1]()  <br/>";
                    /*
                    $view404 = new View();
                    $view404->render('404');
                    return;
                    */
                }
            }
        }


        //---------------------------------------------------------------------------

        // Default Rendering
        $controller->index();


    }

}