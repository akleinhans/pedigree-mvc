<?php


class Message {

    public static function getAll() {

        $messages = Session::get('MESSAGES');

        return ($messages) ? $messages : array();
    }


    public static function add($header, $content, $class = '', $icon = null, $size = 'mini') {

        $messages = Message::getAll();

        array_push($messages, array(
            'header' => $header,
            'content' => $content,
            'class' => $class,
            'icon' => $icon,
            'size' => $size
        ));

        Session::set('MESSAGES', $messages);

    }



    public static function remove($index) {

        $messages = Message::getAll();

        unset($messages[$index]);

        Session::set('MESSAGES', $messages);

    }


}