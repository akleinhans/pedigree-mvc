<?php


class PrivateView extends View {

    public function render($name) {

        if (Session::get('user')) {
            parent::render($name);
        } else {
            Session::set('lastUrl', $_GET['url']);
            header("Location: ".URL."reg");
        }

    }

}