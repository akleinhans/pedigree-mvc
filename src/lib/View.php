<?php


class View {


    //--------------------------------------------

    protected $rendered = false;

    public function render($name) {

        if (!$this->rendered) {
            // inc header
            require "partial/header.php";

            // inc navbar
            require "partial/navbar.php";

            // inc messages
            require "partial/messages.php";

            // inc view($name)
            require "view/$name.php";

            // inc debug
            require "partial/debug.php";

            // inc footer
            require "partial/footer.php";

            $this->rendered = true;
        }

    }

}