<?php

abstract class Controller {

    protected $view;
    protected $model;

    public function __construct() {

        $this->view = new View();

        Session::set('controller', get_class($this));

    }

    public function loadModel() {

        $model_name = get_class($this) . '_Model';
        $model_file = 'model/' . $model_name . '.php';

        if (file_exists($model_file)) {
            require $model_file;
            $this->model = new $model_name;
        }

    }

    abstract public function index();

}