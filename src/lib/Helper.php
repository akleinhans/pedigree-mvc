<?php


class Helper {

    static public function debug($data) {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }

    static public function addUrlParam($name, $value) {

        $fakeUrl =  $_SERVER['REQUEST_URI'];

        $getPos = strpos($fakeUrl, '?');

        if ($getPos) $fakeUrl = substr($fakeUrl, 0, $getPos);

        $params = $_GET;
        unset($params['url']);
        $params[$name] = $value;

        return $fakeUrl . '?' . http_build_query($params);
    }

}