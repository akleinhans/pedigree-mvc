<?php


class Admin extends PrivateController {

    public function showNewItemForm() {
        $this->view->render('admin/item/new');
    }

    public function newItem() {

        $mode = $_GET['mode'];
        $name = trim($_POST['name']);

        if (!$name) {
            header("Location: " . URL . "admin/showNewItemForm?mod=$mode");
            return;
        }

        $image_file = $_FILES['image'];

        if ($image_file) {
            $uploaded_file = File::uploadImg($image_file);
        }   else {
            $uploaded_file = null;
        }

        $this->model->insertRowIntoTable($mode, $name, $uploaded_file);
        Message::add(ucfirst($mode) . ' added', '<code>' . $name . '</code> created as new ' . ucfirst($mode), 'blue', 'cart plus');
        header("Location: " . URL . "admin/showList?mode=$mode");
    }
    /*#######updateEinfuegen##################################

    public function updateRowFromTable($tablename, $id, $name, $uploaded_file) {

        $item = $this->getRowFromTable($tablename, $id);

        if ($uploaded_file) {
            //delete image
            if ($item['image_name'])
                $this->deleteImageEntryAndFiles($item['image_id'], $item['image'], $item['thumb']);
            //new image
            $image_id = $this->insertImage($uploaded_file);
        } else {
            //use old image
            $image_id = $item['image_id'];
        }

        switch ($tablename) {
            case 'colour':
                $sql = 'UPDATE colour SET name = :name, image_id = :image_id WHERE id = :id;';
                break;
            case 'breed':
                $sql = 'UPDATE breed SET name = :name, image_id = :image_id WHERE id = :id;';
                break;
            case 'disease':
                $sql = 'UPDATE disease SET name = :name, image_id = :image_id WHERE id = :id;';
                break;
            case 'gender':
                $sql = 'UPDATE gender SET name = :name, image_id = :image_id WHERE id = :id;';
                break;
            case 'titles':
                $sql = 'UPDATE titles SET name = :name, image_id = :image_id WHERE id = :id;';
                break;

            default: return false;
        }
        $obj = $this->db->prepare($sql);
        $result = $obj->execute(array(
            ':name' => $name,
            ':id' => $id,
            ':image_id' => $image_id
        ));

        return $result;
    }

    public function getRowFromTable($tablename, $id)


    ##########################################*/

    public function updateItem($id) {

        $mode = $_GET['mode'];
        // get name from POST and trim it
        $name = trim($_POST['name']);
        // go back to edit form if name empty
        if (!$name) {
            header("Location: " . URL . "admin/editItem/$id?mode=$mode");
            return;
        }

        $new_file = $_FILES['new_image'];
        if ($new_file['name'] == '') {
            $this->model->updateRowFromTable($mode, $id, $name, null);
        } else {
            $uploaded_file = File::uploadImg($new_file);

            $this->model->updateRowFromTable($mode, $id, $name, $uploaded_file);
        }

        Message::add(ucfirst($mode) . 'updated', '<code>' . $name . '</code> was updated', 'blue', 'wrench');
        header("Location: " . URL . "admin/showList?mode=$mode");
    }

    public function editItem($id) {
        $mode = $_GET['mode'];
        $row = $this->model->getRowFromTable($mode, $id);
        $this->view->row = $row;
        $this->view->render('admin/item/edit');
    }

    public function deleteItem($id) {
        $mode = $_GET['mode'];
        $this->model->deleteFromTable($mode, $id);
        Message::add(ucfirst($mode) . 'deleted', 'ID: <code>' . $id . '</code>deleted', 'blue', 'trash');
        header("Location: " . URL . "admin/showList?mode=$mode");
    }

    public function showList() {
        $mode = $_GET['mode'];

        if (!$mode) {
            header("Location: " . URL . "admin/index");
            return;
        }
        $data = $this->model->getAllFromTable($mode);
        $this->view->data = $data;
        $this->view->render("admin/item/list");
    }

    public function index() {
        $this->view->render('admin/index');
    }
}