<?php


class Reg extends Controller {

 //--------------------Register

    public function register() {
        $this->view->render('reg/register');
    }

    public function doRegister() {
        $error = $this->model->validateForm($_POST);

        if (!$error) {

            $result = $this->model->registerUser($_POST);

            if ($result) header("Location: ".URL."reg/login");

            Message::add('Registration', 'you have succesfully registered', 'green', 'key');

            return;
        }
        $this->view->error = $error;
        $this->view->user = $_POST;
        $this->view->render = ('reg/register');
    }

    //TODO: feedback

    public function index() {
        $this->login();
    }
//--------------------Login---Logout

    public function doLogout() {
        Session::remove('user');

        header("Location ".URL."reg/login");
        return;
    }

    public function login() {
        $this->view->render('reg/login');
    }

    public function doLogin() {

        $user = $this->model->loginUser($_POST);

        if (!$user) {
            $this->view->error = "Email and/or Password are wrong";

            $this->view->render('reg/login');
            return;
        }
            Session::set('user', $user);

            //after Login
            $lastUrl = Session::get('lastUrl');
            $lastUrl = ($lastUrl) ? URL . $lastUrl : URL . 'home';
            Session::remove('lastUrl');

            $firstname = $user['firstname'];
            $lastname = $user['lastname'];

            Message::add('Login', "Welcome back $firstname $lastname", 'green', 'key' );

            header("Location: " . $lastUrl);

            return;
    }

}