<?php

class Owner extends PrivateController {

    public function index() {
        // show all Owner
        $owner = $this->model->getAllOwner();

        $this->view->owner = $owner;

        $this->view->render('owner/index');
    }

    public function search() {

        $search = trim($_GET['search']);

        $this->view->search = $search;
        $this->view->owner = $this->model->getAllOwner($search);

        $this->view->render('owner/index');
    }

    //--------INDEX-----------------------------------------------------------------------------------------------------

    public function newOwner() {

        $name = $this->model->getAllFromTable('name');
        $street = $this->model->getAllFromTable('street');
        $residence = $this->model->getAllFromTable('residence');
        $phone = $this->model->getAllFromTable('phone');
        $email = $this->model->getAllFromTable('email');


        $this->view->name = $name;
        $this->view->street = $street;
        $this->view->residence = $residence;
        $this->view->phone = $phone;
        $this->view->email = $email;

        $this->view->render('owner/newOwner');

    }

    public function addOwner() {

        $formData = $_POST;

        $image_file = $_FILES['image'];

        if ($image_file['name'] != '') {
            $uploaded_file = File::uploadImg($image_file);
        } else {
            $uploaded_file = null;
        }

        $this->model->addOwner($formData, $uploaded_file);

        Message::add('Owner added', '<code>' . $formData['name'] . '</code> created new Owner', 'blue', 'cart plus');

        header("Location: " . URL . "owner");
    }

    public function editOwner($owner_id) {

        $this->view->name = $this->model->getAllFromTable('name');
        $this->view->street = $this->model->getAllFromTable('street');
        $this->view->residence = $this->model->getAllFromTable('residence');
        $this->view->phone = $this->model->getAllFromTable('phone');
        $this->view->email = $this->model->getAllFromTable('email');

        $this->view->owner = $this->model->getDetailForOwner($owner_id);

        $this->view->render('owner/editOwner');

    }

    public function updateOwner($owner_id) {

        header("Location: " . URL . "owner");
    }



    public function deleteOwner($owner_id) {
        // DB delete complete with props and picture
        $this->model->deleteOwner($owner_id);

        header("Location: " . URL . "owner");
    }


}