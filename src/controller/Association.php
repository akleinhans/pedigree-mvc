<?php


class Association extends PrivateController {

    public function index() {
        // show all Associations
        $association = $this->model->getAllAssociation();

        $this->view->association = $association;

        $this->view->render('association/index');
    }

    public function search() {

        $search = trim($_GET['search']);

        $this->view->search = $search;
        $this->view->association = $this->model->getAllAssociation($search);

        $this->view->render('association/index');
    }

    //--------INDEX-----------------------------------------------------------------------------------------------------

    public function newAssociation() {

        $name = $this->model->getAllFromTable('name');
        $street = $this->model->getAllFromTable('street');
        $residence = $this->model->getAllFromTable('residence');
        $phone = $this->model->getAllFromTable('phone');
        $email = $this->model->getAllFromTable('email');


        $this->view->name = $name;
        $this->view->street = $street;
        $this->view->residence = $residence;
        $this->view->phone = $phone;
        $this->view->email = $email;

        $this->view->render('association/newAssociation');

    }

    public function addAssociation() {

        $formData = $_POST;

        $image_file = $_FILES['image'];

        if ($image_file['name'] != '') {
            $uploaded_file = File::uploadImg($image_file);
        } else {
            $uploaded_file = null;
        }

        $this->model->addAssociation($formData, $uploaded_file);

        Message::add('Association added', '<code>' . $formData['name'] . '</code> created new Association entry', 'blue', 'cart plus');

        header("Location: " . URL . "association");
    }

    public function editAssociation($association_id) {

        $this->view->name = $this->model->getAllFromTable('name');
        $this->view->street = $this->model->getAllFromTable('street');
        $this->view->residence = $this->model->getAllFromTable('residence');
        $this->view->phone = $this->model->getAllFromTable('phone');
        $this->view->email = $this->model->getAllFromTable('email');

        $this->view->association = $this->model->getDetailForAssociation($association_id);

        $this->view->render('association/editAssociation');

    }

    public function updateAssociation($association_id) {

        header("Location: " . URL . "association");
    }



    public function deleteDog($association_id) {
        // DB delete complete with props and picture
        $this->model->deleteAssociation($association_id);

        header("Location: " . URL . "association");
    }


}