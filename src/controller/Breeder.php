<?php


class Breeder extends PrivateController {

    public function index() {
        // show all Breeders
        $breeder = $this->model->getAllBreeder();

        $this->view->breeder = $breeder;

        $this->view->render('breeder/index');
    }

    public function search() {

        $search = trim($_GET['search']);

        $this->view->search = $search;
        $this->view->breeder = $this->model->getAllBreeder($search);

        $this->view->render('breeder/index');
    }
    /*##################################################################*/
    public function newBreeder() {

        $name = $this->model->getAllFromTable('name');
        $lastname = $this->model->getAllFromTable('lastname');
        $street = $this->model->getAllFromTable('street');
        $residence = $this->model->getAllFromTable('residence');
        $phone = $this->model->getAllFromTable('phone');
        $email = $this->model->getAllFromTable('email');

        $this->view->name = $name;
        $this->view->lastname = $lastname;
        $this->view->street = $street;
        $this->view->residence = $residence;
        $this->view->phone = $phone;
        $this->view->email = $email;

        $this->view->render('breeder/newBreeder');
    }

    public function addBreeder() {

        $formData = $_POST;

        $image_file = $_FILES['image'];

        if ($image_file['name'] != '') {
            $uploaded_file = File::uploadImg($image_file);
        } else {
            $uploaded_file = null;
        }
        $this->model->addBreeder($formData, $uploaded_file);
        Message::add('Breeder added', '<code>' . $formData['name'] . '</code> created as new Breeder', 'blue', 'cart plus');
        header("Location: " . URL . "breeder");

    }

    public function editBreeder($breeder_id) {

        $this->view->name = $this->model->getAllFromTable('name');
        $this->view->lastname = $this->model->getAllFromTable('lastname');
        $this->view->street = $this->model->getAllFromTable('street');
        $this->view->residence = $this->model->getAllFromTable('residence');
        $this->view->phone = $this->model->getAllFromTable('phone');
        $this->view->email = $this->model->getAllFromTable('email');

        $this->view->breeder = $this->model->getDetailForBreeder($breeder_id);

        $this->view->render('breeder/editBreeder');

    }

    public function updateBreeder($breeder_id) {
        // GET data from $_POST
        // DB update things (caution: new images!! delete old ones etc)

        header("Location: " . URL . "breeder");
    }

    public function deleteBreeder($breeder_id) {
        // DB delete
        $this->model->deleteBreeder($breeder_id);

        header("Location: " . URL . "breeder");
    }

}