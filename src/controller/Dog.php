<?php


class dog extends PrivateController {


    public function search() {

        // search for dogs
        $search = trim($_GET['search']);

        $this->view->search = $search;
        $this->view->dogs = $this->model->getAllDogs($search);

        $this->view->render('dog/index');
    }

    public function index() {

        // show all dogs
        $dogs = $this->model->getAllDogs();

        $this->view->dogs = $dogs;

        $this->view->render('dog/index');

    }

    public function newDog() {

        $association = $this->model->getAllFromTable('association');
        $breed = $this->model->getAllFromTable('breed');
        $breeder = $this->model->getAllFromTable('breeder');
        $colour = $this->model->getAllFromTable('colour');
        $disease = $this->model->getAllFromTable('disease');
        $gender = $this->model->getAllFromTable('gender');
        $owner = $this->model->getAllFromTable('owner');
        $titles = $this->model->getAllFromTable('titles');

        $this->view->association = $association;
        $this->view->breed = $breed;
        $this->view->breeder = $breeder;
        $this->view->colour = $colour;
        $this->view->disease = $disease;
        $this->view->owner = $owner;
        $this->view->titles = $titles;
        $this->view->gender = $gender;

        $this->view->render('dog/newDog');

    }

    public function addDog() {

        $formData = $_POST;

        $image_file = $_FILES['image'];

        if ($image_file['name'] != '') {
            $uploaded_file = File::uploadImg($image_file);
        } else {
            $uploaded_file = null;
        }

        $this->model->addDog($formData, $uploaded_file);

        Message::add('Dog added', '<code>' . $formData['name'] . '</code> created new Dog entry', 'blue', 'cart plus');

        header("Location: " . URL . "dog");
    }

    public function editDog($dog_id) {

        $this->view->colour = $this->model->getAllFromTable('colour');
        $this->view->gender = $this->model->getAllFromTable('gender');
        $this->view->disease = $this->model->getAllFromTable('disease');
        $this->view->titles = $this->model->getAllFromTable('titles');
        $this->view->breed = $this->model->getAllFromTable('breed');
        $this->view->breeder = $this->model->getAllFromTable('breeder');
        $this->view->owner = $this->model->getAllFromTable('owner');
        $this->view->association = $this->model->getAllFromTable('association');
        $this->view->birthdate = $this->model->getAllFromTable('birthdate');
        $this->view->registration = $this->model->getAllFromTable('registration');

        $this->view->dog = $this->model->getDetailForDog($dog_id);

        $this->view->render('dog/editDog');

    }

    public function updateDog($dog_id) {

        header("Location: " . URL . "dog/showDetail/" . $dog_id);
    }

    public function showDetail($dog_id) {

        $this->view->dog = $this->model->getDetailForDog($dog_id);

        $this->view->render('dog/detail');
    }

    public function deleteDog($dog_id) {
        // DB delete complete with props and picture
        $this->model->deleteDog($dog_id);

        header("Location: " . URL . "dog");
    }




}