<?php
$breeder = $this->breeder;

$search = isset($this->search) ? $this->search : null;

$header = ($search) ? "Search results for <code>$search</code>" : 'breeder';
?>

<h3 class="ui header">Dog Breeder</h3>

<?php if(count($breeder) > 0): ?>

    <table class="ui celled table segment">
        <thead>
        <tr>
            <?php foreach ($breeder[0] as $key => $value): ?>
                <th><?= $key ?></th>
            <?php endforeach; ?>
            <th>actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($breeder as $row): ?>
            <tr>
                <?php foreach ($row as $key => $value): ?>
                    <?php if($key == 'thumb'): ?>
                        <td data-label="<?= $key ?>">
                            <?php if($value): ?>
                                <img src="<?= URL . $value ?>">
                            <?php else: ?>
                                <img src="<?= URL . IMAGE_PLACEHOLDER_THUMB ?>">
                            <?php endif; ?>
                        </td>
                    <?php else: ?>
                        <td data-label="<?= $key ?>"><?= $value ?></td>
                    <?php endif; ?>
                <?php endforeach; ?>

                <td>
                    <a id="deleteBtn" class="ui icon button deleteBtn" data-name="<?=$row['id']?>" data-href="<?=URL?>breeder/deleteBreeder">
                        <i class="trash icon"></i>
                    </a>
                    <a class="ui icon button" href="<?=URL?>breeder/editBreeder/<?=$row['id']?>">
                        <i class="edit icon"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>



<?php endif; ?>



<a class="ui button" href="<?=URL?>breeder/newBreeder">New Breeder</a></h3>


<!-- ############################################################ -->

<script>
    let thumbPreview = document.querySelector('#thumbPreview');

    let deleteButton = document.querySelector('#deleteBtn');
    let confirmDeleteButton = document.querySelector('#confirmDelete');
    let deleteMsgName = document.querySelector('#deleteMsgName');

    thumbPreview.addEventListener('click', () => {
        $('.ui.modal.bigImage').modal('show');
    });

    deleteButton.addEventListener('click', function() {
        confirmDeleteButton.href = this.getAttribute('data-href');
        deleteMsgName.innerHTML = this.getAttribute('data-name');
        $('.ui.modal.deleteMsg').modal('show');
    });

</script>