<?php
$name = $this->name;
$lastname = $this->lastname;
$street = $this->street;
$residence = $this->residence;
$phone = $this->phone;
$email = $this->email;

?>

<h1>Add a New Breeder</h1>

<a class="ui button" href="<?=URL?>breeder">Back</a>

<h3 class="ui header">New Breeder</h3>



<form class="ui form" method="post" action="<?=URL?>breeder/addBreeder" enctype="multipart/form-data">
    <div class="two fields">
        <div class="field">
            <label>Name</label>
            <input type="text" name="name" value="">
        </div>
        <div class="field">
            <label>Lastname</label>
            <input type="text" name="lastname" value="">
        </div>
    </div>
    <div class="two fields">
        <div class="field">
            <label>Street</label>
            <input type="text" name="street" value="">
        </div>
        <div class="field">
            <label>Residence</label>
            <input type="text" name="residence" value="">
        </div>
    </div>
    <div class="two fields">
        <div class="field">
            <label>Phone</label>
            <input type="text" name="phone" value="">
        </div>
        <div class="field">
            <label>Email</label>
            <input type="text" name="email" value="">
        </div>
    </div>

    <input type="hidden" name="MAX_FILE_SIZE" value="6000000">

    <div class="field">
        <label>Image</label>
        <input type="file" name="image" >
    </div>

    <button class="ui button" type="submit">Submit</button>
</form>