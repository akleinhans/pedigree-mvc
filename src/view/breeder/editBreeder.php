<?php
$name = $this->name;
$lastname = $this->lastname;
$street = $this->street;
$residence = $this->residence;
$phone = $this->phone;
$email = $this->email;

$breeder = $this->breeder;

function isIdInArray($array, $id) {
    foreach ($array as $row) {
        if ($row['id'] == $id ) return true;
    }
    return false;
}
?>

<a class="ui button" href="<?=URL?>breeder">Back</a>

<h3 class="ui header">Edit Breeder</h3>

<form class="ui form" method="post" action="<?=URL?>breeder/updateBreeder/<?=$breeder['id']?>" enctype="multipart/form-data">
    <div class="two fields">
        <div class="field">
            <label>Name</label>
            <input type="text" name="name" value="<?=$breeder['name']?>">
        </div>
        <div class="field">
            <label>Lastname</label>
            <input type="text" name="lastname" value="<?=$breeder['lastname']?>">
        </div>
    </div>
    <div class="two fields">
        <div class="field">
            <label>Street</label>
            <input type="text" name="street" value="<?=$breeder['street']?>">
        </div>
        <div class="field">
            <label>Residence</label>
            <input type="text" name="residence" value="<?=$breeder['residence']?>">
        </div>
    </div>
    <div class="two fields">
        <div class="field">
            <label>Phone</label>
            <input type="text" name="phone" value="<?=$breeder['phone']?>">
        </div>
        <div class="field">
            <label>Email</label>
            <input type="text" name="email" value="<?=$breeder['email']?>">
        </div>
    </div>


    <input type="hidden" name="MAX_FILE_SIZE" value="6000000">


    <div class="field">
        <label>Image</label>
        <img src="<?=URL . $breeder['thumb']?>">
    </div>

    <div class="field">
        <label>New Image</label>
        <input type="file" name="image" >
    </div>

    <button class="ui button" type="submit">Submit</button>
</form>
