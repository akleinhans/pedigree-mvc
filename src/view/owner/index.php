<?php
$owner = $this->owner;

$search = isset($this->search) ? $this->search : null;

$header = ($search) ? "Search results for <code>$search</code>" : 'owner';
?>

<h3 class="ui header">Dog Owners</h3>

<?php if(count($owner) > 0): ?>

    <table class="ui celled table">
        <thead>
        <tr>
            <?php foreach ($owner[0] as $key => $value): ?>
                <th><?= $key ?></th>
            <?php endforeach; ?>
            <th>actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($owner as $row): ?>
            <tr>
                <?php foreach ($row as $key => $value): ?>
                    <?php if($key == 'thumb'): ?>
                        <td data-label="<?= $key ?>">
                            <?php if($value): ?>
                                <img src="<?= URL . $value ?>">
                            <?php else: ?>
                                <img src="<?= URL . IMAGE_PLACEHOLDER_THUMB ?>">
                            <?php endif; ?>
                        </td>
                    <?php else: ?>
                        <td data-label="<?= $key ?>"><?= $value ?></td>
                    <?php endif; ?>
                <?php endforeach; ?>

                <td>
                    <a class="ui icon button" id="deleteBtn" data-name="<?=$owner['name']?>" data-href="<?=URL?>owner/deleteOwner/<?=$owner['id']?>">
                        <i class="trash icon"></i>
                    </a>
                    <a class="ui icon button" href="<?=URL?>owner/editOwner/<?=$row['id']?>">
                        <i class="edit icon"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php endif; ?>

<div class="ui modal deleteMsg">
    <div class="header">Caution</div>
    <div class="content">
        <p>Do you want to delete the Dog <span id="deleteMsgName">XYZ</span></p>
    </div>
    <div class="actions">
        <a id="confirmDelete" class="ui approve button red">Delete</a>
        <a class="ui cancel button">Cancel</a>
    </div>
</div>

<a class="ui button" href="<?=URL?>owner/newOwner">New Owner</a>

<script>
    let thumbPreview = document.querySelector('#thumbPreview');

    let deleteButton = document.querySelector('#deleteBtn');
    let confirmDeleteButton = document.querySelector('#confirmDelete');
    let deleteMsgName = document.querySelector('#deleteMsgName');

    thumbPreview.addEventListener('click', () => {
        $('.ui.modal.bigImage').modal('show');
    });

    deleteButton.addEventListener('click', function() {
        confirmDeleteButton.href = this.getAttribute('data-href');
        deleteMsgName.innerHTML = this.getAttribute('data-name');
        $('.ui.modal.deleteMsg').modal('show');
    });

</script>