<?php
$name = $this->name;
$street = $this->street;
$residence = $this->residence;
$phone = $this->phone;
$email = $this->email;

$owner = $this->owner;

?>

<a class="ui button" href="<?=URL?>owner">Back</a>

<h3 class="ui header">Edit Owner</h3>

<form class="ui form" method="post" action="<?=URL?>owner/updateOwner/<?=$owner['id']?>" enctype="multipart/form-data">
    <div class="two fields">
        <div class="field">
            <label>Name</label>
            <input type="text" name="name" value="<?=$owner['name']?>">
        </div>
        <div class="field">
            <label>Nickname</label>
            <input type="text" name="street" value="<?=$owner['street']?>">
        </div>
    </div>
    <div class="two fields">
        <div class="field">
            <label>Residence</label>
            <input type="text" name="residence" value="<?=$owner['residence']?>">
        </div>
        <div class="field">
            <label>Phone</label>
            <input type="text" name="phoned" value="<?=$owner['phone']?>">
        </div>
    </div>

    <input type="hidden" name="MAX_FILE_SIZE" value="6000000">

    <div class="field">
        <label>email</label>
        <input type="text" name="email" value="<?=$owner['email']?>">
    </div>
    <div class="field">
        <label>Image</label>
        <img src="<?=URL . $owner['thumb']?>">
    </div>
    <div class="field">
        <label>New Image</label>
        <input type="file" name="image" >
    </div>

    <button class="ui button" type="submit">Submit</button>
</form>