
<h1>Admin</h1>

<div class="adminContainer">


<a class="ui button" href="<?=URL?>dog/newDog">New Dog</a>
<a class="ui button" href="<?=URL?>breeder/newBreeder">New Breeder</a>
<a class="ui button" href="<?=URL?>owner/newOwner">New Owner</a>
<a class="ui button" href="<?=URL?>association/newAssociation">New Association</a>
<a class="ui secondary button" href="<?=URL?>admin/showList?mode=breed">Breed</a>
<a class="ui secondary button" href="<?=URL?>admin/showList?mode=colour">Colour</a>
<a class="ui secondary button" href="<?=URL?>admin/showList?mode=disease">Disease</a>
<a class="ui secondary button" href="<?=URL?>admin/showList?mode=gender">Gender</a>
<a class="ui secondary button" href="<?=URL?>admin/showList?mode=titles">Titles</a>
</div>