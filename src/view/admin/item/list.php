<?php
$mode = $_GET['mode'];

$data = $this->data;

Debug::add($data);

?>

<a class="ui button" href="<?=URL?>admin">Back</a>

<h3 class="ui header">All <?=ucfirst($mode)?></h3>

<?php if(count($data) > 0): ?>

    <table class="ui celled table">
        <thead>
        <tr>
            <?php foreach ($data[0] as $key => $value): ?>
                <th><?= $key ?></th>
            <?php endforeach; ?>
            <th>actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $row): ?>
            <tr>
                <?php foreach ($row as $key => $value): ?>
                    <?php if($key == 'thumb'): ?>
                        <td data-label="<?= $key ?>">
                            <?php if($value): ?>
                                <img src="<?= URL . $value ?>">
                            <?php else: ?>
                                <img src="<?= URL . IMAGE_PLACEHOLDER_THUMB ?>">
                            <?php endif; ?>
                        </td>
                    <?php else: ?>
                        <td data-label="<?= $key ?>"><?= $value ?></td>
                    <?php endif; ?>
                <?php endforeach; ?>

                <td>
                    <a class="ui icon button deleteBtn" data-name="<?=$row['name']?>" data-href="<?=URL?>admin/deleteItem/<?=$row['id']?>?mode=<?=$mode?>">
                        <i class="trash icon"></i>
                    </a>
                    <a class="ui icon button" href="<?=URL?>admin/editItem/<?=$row['id']?>?mode=<?=$mode?>">
                        <i class="edit icon"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php endif; ?>

<a class="ui button" href="<?=URL?>admin/showNewItemForm?mode=<?=$mode?>">New <?=ucfirst($mode)?></a>

<!-- ############################################################################################################### -->

<div class="ui modal deleteMsg">
    <div class="header">Caution</div>
    <div class="content">
        <p>Do you want to delete <?=ucfirst($mode)?> <span id="deleteMsgName">XYZ</span></p>
    </div>
    <div class="actions">
        <a id="confirmDelete" class="ui approve button red">Delete</a>
        <a class="ui cancel button">Cancel</a>
    </div>
</div>

<!-- ############################################################################################################### -->

<script>

    let allDeleteButtons = document.querySelectorAll('.deleteBtn');
    let confirmDeleteButton = document.querySelector('#confirmDelete');
    let deleteMsgName = document.querySelector('#deleteMsgName');

    allDeleteButtons.forEach((elem) => {
        elem.addEventListener('click', function() {
            confirmDeleteButton.href = this.getAttribute('data-href');
            deleteMsgName.innerHTML = this.getAttribute('data-name');
            $('.ui.modal.deleteMsg').modal('show');
        });
    });

</script>
