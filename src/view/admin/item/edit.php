<?php

$mode = $_GET['mode'];

$actionUrl = URL . "admin/updateItem/" . $this->row['id'] . "?mode=" . $mode;
$item = $this->row;
$header = 'Edit';

$item['thumb'] = ($item['thumb']) ? $item['thumb'] : IMAGE_PLACEHOLDER_THUMB;

?>

<a class="ui button" href="<?=URL?>admin/showList?mode=<?=$mode?>">Back</a>

<h3 class="ui header"><?=$header?> <?=ucfirst($mode)?></h3>

<form class="ui form" method="post" action="<?=$actionUrl?>" enctype="multipart/form-data">
    <div class="field">
        <label>Name</label>
        <input type="text" name="name" value="<?=$item['name']?>">
    </div>

    <div class="field">
        <label>Image</label>
        <img src="<?=URL . $item['thumb']?>">
    </div>

    <input type="hidden" name="MAX_FILE_SIZE" value="6000000">

    <div class="field">
        <label>New Image</label>
        <input type="file" name="new_image" >
    </div>

    <button class="ui button" type="submit">Submit</button>
</form>

