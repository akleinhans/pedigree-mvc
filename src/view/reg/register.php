<?php
$user['firstname'] = isset($this->user['firstname']) ? $this->user['firstname'] : '';
$user['lastname'] = isset($this->user['lastname']) ? $this->user['lastname'] : '';
$user['street'] = isset($this->user['lastname']) ? $this->user['street'] : '';
$user['postalcode'] = isset($this->user['lastname']) ? $this->user['postalcode'] : '';
$user['residence'] = isset($this->user['lastname']) ? $this->user['residence'] : '';
$user['phone'] = isset($this->user['lastname']) ? $this->user['phone'] : '';
$user['email'] = isset($this->user['email']) ? $this->user['email'] : '';
$user['password'] = isset($this->user['password']) ? $this->user['password'] : '';

$error = isset($this->error) ? $this->error : null;

$errorLabels = array();

if ($error)
    foreach ($error as $key => $value) {
        $errorLabels[$key] = "<div class=\"ui pointing red basic label\">$value</div>";
    }

?>

<h1>Register</h1>

<?php if ($error): ?>
    <div class="ui warning message">
        <i class="close icon"></i>
        <div class="header">
            Validation Error!
        </div>
        <ul>
            <?php foreach ($error as $key => $value): ?>
                <li><b><?= ucfirst($key) ?>:</b> <?= $value ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<form class="ui form" method="post" action="<?=URL?>reg/doregister">
    <div class="field">
        <label>Firstname</label>
        <input type="text" name="firstname" value="<?= $user['firstname'] ?>">
        <?= isset($errorLabels['firstname']) ? $errorLabels['firstname'] : '' ?>
    </div>
    <div class="field">
        <label>Lastname</label>
        <input type="text" name="lastname" value="<?= $user['lastname'] ?>">
        <?= isset($errorLabels['lastname']) ? $errorLabels['lastname'] : '' ?>
    </div>
    <div class="field">
        <label>Street</label>
        <input type="text" name="street" value="<?= $user['street'] ?>">
        <?= isset($errorLabels['street']) ? $errorLabels['street'] : '' ?>
    </div>
    <div class="field">
        <label>Postalcode</label>
        <input type="text" name="postalcode" value="<?= $user['postalcode'] ?>">
        <?= isset($errorLabels['postalcode']) ? $errorLabels['postalcode'] : '' ?>
    </div>
    <div class="field">
        <label>Residence</label>
        <input type="text" name="residence" value="<?= $user['residence'] ?>">
        <?= isset($errorLabels['residence']) ? $errorLabels['residence'] : '' ?>
    </div>
    <div class="field">
        <label>Phone</label>
        <input type="text" name="phone" value="<?= $user['phone'] ?>">
        <?= isset($errorLabels['phone']) ? $errorLabels['phone'] : '' ?>
    </div>
    <div class="field">
        <label>Email</label>
        <input type="text" name="email" value="<?= $user['email'] ?>">
        <?= isset($errorLabels['email']) ? $errorLabels['email'] : '' ?>
    </div>
    <div class="field">
        <label>Password</label>
        <input type="text" name="password" value="<?= $user['password'] ?>">
        <?= isset($errorLabels['password']) ? $errorLabels['password'] : '' ?>
    </div>
    <div class="field">
        <label>Password Confirm</label>
        <input type="text" name="password2" >
        <?= isset($errorLabels['password2']) ? $errorLabels['password2'] : '' ?>
    </div>
    <button class="ui button" type="submit">Submit</button>
</form>

<div class="ui floating message">
    <p>Allready registered ? Go to <a class="ui button" href="<?=URL?>reg/login">Login</a></p>
</div>

