<?php
    $error = isset($this->error) ? $this->error : null;
?>

<h1>Login</h1>

<?php if (isset($this->error)): ?>
    <div class="ui message error">
        <div class="header!"><?=$this->error?></div>
    </div>
<?php endif; ?>

<form class="ui form" method="post" action="<?=URL?>reg/doLogin">
    <div class="field">
        <label>Email</label>
        <input type="text" name="email">
    </div>
    <div class="field">
        <label>Password</label>
        <input type="text" name="password">
    </div>
    <button class="ui button" type="submit">Submit</button>
</form>

<div class="ui floating message">
    <p>Not registered yet? Create an account here <a class="ui button" href="<?=URL?>reg/register">Register</a></p>
</div>