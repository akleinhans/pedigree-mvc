<?php

$dogs = $this->dogs;

$search = isset($this->search) ? $this->search : null;

$header = ($search) ? "Search results for <code>$search</code>" : 'dogs';
?>


<h3 class="ui header"><?=$header?></h3>

<?php if ($search && count($dogs) == 0): ?>
    <p>No dogs found</p>
<?php else: ?>

    <div class="ui four cards">
        <?php foreach ($dogs as $row): ?>

            <div class="ui card">
                <!--link-detailSite---->
                <a class="header" href="<?=URL?>dog/showDetail/<?=$row['id']?>">
                    <!--picture--or--thump---->
                    <div class="image" style="height: 266px; background-color: white;">
                        <?php if ($row["thumb"]): ?>
                            <div style="width:100%; height:100%; background: url(<?=URL . $row["thumb"]?>) center center no-repeat"></div>
                        <?php else: ?>
                            <div style="width:100%; height:100%; background: url(<?=URL . IMAGE_PLACEHOLDER_THUMB?>) center center no-repeat"></div>
                        <?php endif; ?>
                    </div>
                </a>
                <div class="content">
                    <a class="header" href="<?=URL?>dog/showDetail/<?=$row['id']?>"><?=$row['name']?></a>
                    <div class="meta">
                        <div class="ui label">
                            <i class="heterosexual icon"></i> <?=$row["gender_name"]?>
                        </div>
                        <div class="ui label">
                            <i class="tint icon"></i> <?=$row["colour_name"]?>
                        </div>
                    </div>
                    <div class="description">
                        <?=$row['text']?>
                    </div>
                </div>
                <div class="extra content">
                    <?php foreach ($row['disease'] as $disease_item): ?>
                        <div class="ui label proplist">
                            <i class="heartbeat icon"></i> <?=$disease_item['name']?>
                        </div>
                    <?php endforeach; ?>
                    <br/>
                    <?php foreach ($row['titles'] as $titles_item): ?>
                        <div class="ui label proplist">
                            <i class="trophy icon"></i> <?=$titles_item['name']?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>


        <?php endforeach; ?>
    </div>

<?php endif; ?>

<br/>

<a class="ui button" href="<?=URL?>dog/newDog">New Dogs</a>