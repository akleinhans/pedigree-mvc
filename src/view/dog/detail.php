<?php

$dog = $this->dog;

?>



<div class="ui grid container">
    <div class="eight wide column">
        <a class="ui button secondary" href="<?=URL?>dog">Back</a>
    </div>
    <div class="eight wide column right aligned">
            <a class="ui  button secondary " id="deleteBtn" data-name="<?=$dog['name']?>" data-href="<?=URL?>dog/deleteDog/<?=$dog['id']?>">Delete</a>
            <a class="ui  button secondary" href="<?=URL?>dog/editDog/<?=$dog['id']?>">Edit</a>
    </div>
</div>

<br/><br/>
    <div class="ui equal width padded grid">

            <div id="thumbPreview" class="thumb four wide top aligned column grey" style="width: 300px; height: 300px; background-color: white;">
                <?php if ($dog["thumb"]): ?>
                    <div style="width:100%; height:100%; background: url(<?=URL . $dog["thumb"]?>) center center no-repeat"></div>
                <?php else: ?>
                    <div style="width:100%; height:100%; background: url(<?=URL . IMAGE_PLACEHOLDER_THUMB?>) center center no-repeat"></div>
                <?php endif; ?>
            </div>

            <div class="content twelve wide column grey">
                <a class="ui secondary header">Name: <?=$dog['name']?></a><br/>
                <div class="header">
                    <a class="ui secondary header">Nickname: <?=$dog['Nickname']?></a>
                </div><br/>
                <div class="description">
                    <?=$dog['text']?>
                </div>
            </div>
    </div>

<div class="ui divider"></div>

    <div class="ui equal width center aligned padded grid">
        <div class="eight wide column grey">
            <a class="ui secondary header">Disease:<br/><br/>
                <?php foreach ($dog['disease'] as $disease_item): ?>
                    <div class="ui label proplist">
                        <i class="heartbeat icon"></i> <?=$disease_item['name']?>
                    </div>
                <?php endforeach; ?>
            </a>
        </div>
        <div class="eight wide column grey">
            <a class="ui secondary header">Titles:<br/><br/>
                <?php foreach ($dog['titles'] as $titles_item): ?>
                    <div class="ui label proplist">
                        <i class="trophy icon"></i> <?=$titles_item['name']?>
                    </div>
                <?php endforeach; ?>
            </a>
        </div>
    </div>

<div class="ui divider"></div>

    <div class="ui equal width center aligned padded grid">
        <div class="four wide column grey">
            <a class="ui secondary header">Gender:<br/><br/> <?=$dog['gender_name']?></a>
        </div>
        <div class="four wide column grey">
            <a class="ui secondary header">Colour:<br/><br/> <?=$dog['colour_name']?></a>
        </div>
        <div class="four wide column grey">
            <a class="ui secondary header">Birthdate:<br/><br/> <?=$dog['birth_date']?></a>
        </div>
        <div class="four wide column grey">
            <a class="ui secondary header">Breed:<br/><br/> <?=$dog['breed_name']?></a>
        </div>
    </div>

<div class="ui divider"></div>

    <div class="ui equal width center aligned padded grid">
        <div class="four wide column grey">
            <a class="ui secondary header">Association:<br/><br/> <?=$dog['association_name']?></a>
        </div>
        <div class="four wide column grey">
            <a class="ui secondary header">Registration No.:<br/><br/> <?=$dog['reg_id']?></a>
        </div>
        <div class="four wide column grey">
            <a class="ui secondary header">Breeder:<br/><br/> <?=$dog['breeder_name']?></a>
        </div>
        <div class="four wide column grey">
            <a class="ui secondary header">Owner:<br/><br/> <?=$dog['owner_name']?></a>
        </div>
    </div>
<div class="ui divider"></div>
    <div class="ui equal width center aligned padded grid">
        <div class="ui sixteen wide column grey">
            <a class="ui secondary header">Text:<br/><br/> <?=$dog['text']?></a>
        </div>
    </div>

<div class="ui modal deleteMsg">
    <div class="header">Caution</div>
    <div class="content">
        <p>Do you want to delete the Dog <span id="deleteMsgName">XYZ</span></p>
    </div>
    <div class="actions">
        <a id="confirmDelete" class="ui approve button red">Delete</a>
        <a class="ui cancel button">Cancel</a>
    </div>
</div>

<!-- ################Scripts############################# -->

<script>
    let thumbPreview = document.querySelector('#thumbPreview');

    let deleteButton = document.querySelector('#deleteBtn');
    let confirmDeleteButton = document.querySelector('#confirmDelete');
    let deleteMsgName = document.querySelector('#deleteMsgName');

    thumbPreview.addEventListener('click', () => {
        $('.ui.modal.bigImage').modal('show');
    });

    deleteButton.addEventListener('click', function() {
        confirmDeleteButton.href = this.getAttribute('data-href');
        deleteMsgName.innerHTML = this.getAttribute('data-name');
        $('.ui.modal.deleteMsg').modal('show');
    });

</script>