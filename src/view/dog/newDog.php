<?php

$breed = $this->breed;
$colour = $this->colour;
$disease = $this->disease;
$gender = $this->gender;
$titles = $this->titles;
$owner = $this->owner;
$breeder = $this->breeder;
$association = $this->association;


?>
<h1>Add a New Dog</h1>

<a class="ui secondary button" href="<?=URL?>dog">Back</a>

<h3 class="ui header">New Dog</h3>

<form class="ui form" method="post" action="<?=URL?>dog/addDog" enctype="multipart/form-data">
    <div class="two fields">
        <div class="field">
            <label>Name</label>
            <input type="text" name="name" value="">
        </div>
        <div class="field">
            <label>Nickname</label>
            <input type="text" name="Nickname" value="">
        </div>
    </div>

    <div class="two fields">
        <div class="field">
            <label>Birthdate</label>
            <input type="text" name="birth_date" value="">
        </div>
        <div class="field">
            <label>Registration No.</label>
            <input type="text" name="reg_id" value="">
        </div>
    </div>

    <div class="two fields">
        <div class="field">
            <label>Gender</label>
            <select class="ui fluid dropdown" name="gender" >
                <option value=""></option>
                <?php foreach ($gender as $row): ?>
                    <option value="<?=$row['id']?>"><?=$row['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="field">
            <label>Colour</label>
            <select class="ui fluid dropdown" name="colour">
                <option value=""></option>
                <?php foreach ($colour as $row): ?>
                    <option value="<?=$row['id']?>"><?=$row['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>



    <div class="two fields">
        <div class="field">
            <label>Titles</label>
            <select class="ui fluid dropdown" name="titles[]" multiple="">
                <option value=""></option>
                <?php foreach ($titles as $row): ?>
                    <option value="<?=$row['id']?>"><?=$row['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="field">
            <label>Disease</label>
            <select class="ui fluid dropdown" name="disease[]" multiple="">
                <option value=""></option>
                <?php foreach ($disease as $row): ?>
                    <option value="<?=$row['id']?>"><?=$row['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="two fields">
        <div class="field">
            <label>Breeder</label>
            <select class="ui fluid dropdown" name="breeder" >
                <option value=""></option>
                <?php foreach ($breeder as $row): ?>
                    <option value="<?=$row['id']?>"><?=$row['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="field">
            <label>Owner</label>
            <select class="ui fluid dropdown" name="owner" ">
                <option value=""></option>
                <?php foreach ($owner as $row): ?>
                    <option value="<?=$row['id']?>"><?=$row['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="two fields">
        <div class="field">
            <label>Association</label>
            <select class="ui fluid dropdown" name="association" >
                <option value=""></option>
                <?php foreach ($association as $row): ?>
                    <option value="<?=$row['id']?>"><?=$row['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="field">
            <label>Breed</label>
            <select class="ui fluid dropdown" name="breed"  >
                <option value=""></option>
                <?php foreach ($breed as $row): ?>
                    <option value="<?=$row['id']?>"><?=$row['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>


    <input type="hidden" name="MAX_FILE_SIZE" value="6000000">

    <div class="field">
        <label>Text</label>
        <textarea class="tinymce-editor" name="text"></textarea>
    </div>

    <div class="field">
        <label>Image</label>
        <input type="file" name="image" >
    </div>

    <button class="ui button" type="submit">Submit</button>
</form>
