<?php
$name = $this->name;
$street = $this->street;
$residence = $this->residence;
$phone = $this->phone;
$email = $this->email;
?>
<h1>Add a New Association</h1>

<a class="ui secondary button" href="<?=URL?>association">Back</a>

<h3 class="ui header">New Association</h3>

<form class="ui form" method="post" action="<?=URL?>association/addAssociation" enctype="multipart/form-data">
    <div class="two fields">
        <div class="field">
            <label>Name</label>
            <input type="text" name="name" value="">
        </div>
        <div class="field">
            <label>street</label>
            <input type="text" name="street" value="">
        </div>
    </div>

    <div class="two fields">
        <div class="field">
            <label>Residence</label>
            <input type="text" name="residence" value="">
        </div>
        <div class="field">
            <label>Phone</label>
            <input type="text" name="phone" value="">
        </div>
    </div>








    <input type="hidden" name="MAX_FILE_SIZE" value="6000000">

    <div class="field">
        <label>Email</label>
        <input type="text" name="email" value="">
    </div>

    <div class="field">
        <label>Image</label>
        <input type="file" name="image" >
    </div>

    <button class="ui button" type="submit">Submit</button>
</form>