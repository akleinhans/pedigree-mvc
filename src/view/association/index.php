<?php
$association = $this->association;

$search = isset($this->search) ? $this->search : null;

$header = ($search) ? "Search results for <code>$search</code>" : 'association';
?>

<h3 class="ui header">Dog association</h3>

<?php if(count($association) > 0): ?>

    <table class="ui celled table">
        <thead>
        <tr>
            <?php foreach ($association[0] as $key => $value): ?>
                <th><?= $key ?></th>+
            <?php endforeach; ?>
            <th>actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($association as $row): ?>
            <tr>
                <?php foreach ($row as $key => $value): ?>
                    <?php if($key == 'thumb'): ?>
                        <td data-label="<?= $key ?>">
                            <?php if($value): ?>
                                <img src="<?= URL . $value ?>">
                            <?php else: ?>
                                <img src="<?= URL . IMAGE_PLACEHOLDER_THUMB ?>">
                            <?php endif; ?>
                        </td>
                    <?php else: ?>
                        <td data-label="<?= $key ?>"><?= $value ?></td>
                    <?php endif; ?>
                <?php endforeach; ?>

                <td>
                    <a class="ui icon button deleteBtn" data-name="<?=$row['name']?>" data-href="<?=URL?>association/deleteItem">
                        <i class="trash icon"></i>
                    </a>
                    <a class="ui icon button" href="<?=URL?>association/editAssociation/<?=$row['id']?>">
                        <i class="edit icon"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php endif; ?>

<a class="ui button" href="<?=URL?>association/newAssociation">New Association</a></h3>